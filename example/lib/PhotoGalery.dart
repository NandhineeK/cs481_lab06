import 'package:flutter/material.dart';
import 'package:cs481_lab06/main.dart';
import 'dart:math';

import 'main.dart';

class PhotoGalery extends StatefulWidget {
  @override
  _PhotoGaleryState createState() => _PhotoGaleryState();
}

class _PhotoGaleryState extends State<PhotoGalery> {

  static const List<String> _title = [
    'South Montain',
    'Liberal Tree',
    'Sunset beach',
    'Abstract painting'
  ];

  static const List<String> _imgs = [
    'https://stylegesture.com/wp-content/uploads/2019/02/Simple-Watercolor-Painting-Ideas-For-Beginners-2-1.jpg',
    'https://watercolorpainting.com/staging/wp-content/uploads/2018/11/Surreal-Skies.png',
    'https://secure.img1-fg.wfcdn.com/im/45417902/resize-h800%5Ecompr-r85/4438/44387010/%2527Hawaiian+Sunset%2527+Acrylic+Painting+Print+on+Canvas.jpg',
    'https://i.pinimg.com/564x/c1/2f/18/c12f189dfc84589d8db8c64b693b7c32.jpg'
  ];

  double x = 0;
  double y = 0;
  double z = 0.002;

  bool myScale = false;

  void initSate() {
    super.initState();
    for (int i = 0; i <= _imgs.length; i++) {
    }
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Photo Galery'),
        backgroundColor: Colors.red,
      ),
      body: ListView.builder(
        itemCount: _imgs.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) =>
        Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
         Text(_title[index]),
            Transform(
              transform: Matrix4(
                1,0,0,0.001,
                0,1,0,0,
                0,0,1,z,
                0,0,0,1,
              )..rotateX(x)..rotateY(y)..rotateZ(z),
              alignment: FractionalOffset.center,
              child: GestureDetector(
                onPanUpdate: (details) {
                  setState(() {
                    y = y - details.delta.dx / 100;
                    x = x + details.delta.dy / 100;
                  });
                },
                child: AnimatedContainer(
                  padding: EdgeInsets.all(20),
                  width: 350,
                  height: 350,
                  duration: Duration(milliseconds: 200),
                  child: Image.network(_imgs[index]),
                ),
              ),
            ),






           // GestureDetector(
          //onTap: () => {
           // print(_title[index])
          //},
         //
             // AnimatedContainer(
               // padding: EdgeInsets.all(20),
               // width: 200,
               // height: 200,
              //  duration: Duration(milliseconds: 200),
              //  child: Image.network(_imgs[index]),
              //),
            ],
          ),
        ),
      //),
    );
  }
}
