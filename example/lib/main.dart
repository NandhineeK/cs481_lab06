import 'dart:async';
// import 'package:cs481_lab06/MyFadeApp.dart';
import 'package:flutter/material.dart';

import 'MyFadeApp.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Tutorial',
        initialRoute: '/',
        routes: {
          // When navigating to the "/" route, build the FirstScreen widget.
          '/': (context) => _CircularProgressIndicatorApp(),
          // When navigating to the "/second" route, build the SecondScreen widget.
          '/second': (context) => MyFadeApp(),
        }
      //home: _CircularProgressIndicatorApp(),
    );
  }
}
class _CircularProgressIndicatorApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CircularProgressIndicatorAppState();
  }
}

class _CircularProgressIndicatorAppState
    extends State<_CircularProgressIndicatorApp> {
  double _progress = 0;

  void startTimer() {
    new Timer.periodic(
      Duration(seconds: 1),
          (Timer timer) =>
          setState(
                () {
              if (_progress == 1) {
                timer.cancel();
                Navigator.pushNamed(context, '/second');
                //Navigator.push(
                  //context,
                  //MaterialPageRoute(builder: (context) => MyHomePage()),
                //);
              } else {
                _progress += 0.2;
              }
            },
          ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text('Photo Gallary'),
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(15.0),
          child: Column(
            children: <Widget>[
              Padding(padding: EdgeInsets.fromLTRB(0, 50, 0, 0),),
              Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQeDSCfmPvKH3z0iHc42mZG5sdvq4kISK7njw&usqp=CAU'),
              Padding(padding: EdgeInsets.fromLTRB(0, 50, 0, 0),),
              Text('Hello Friends, Welcome to our Photo Gallary',textAlign:TextAlign.center,style: TextStyle(color: Colors.white,fontSize: 25,fontWeight: FontWeight.w900,),),
              Padding(padding: EdgeInsets.fromLTRB(0, 30, 0, 0),),
              Text('Everywhere you walk, every place you go is full of art, explicit or hidden! If you can see them, you will be the richest art collector and your memory will be the richest art gallery!',textAlign:TextAlign.center,style: TextStyle(color: Colors.yellowAccent,fontSize: 20,fontWeight: FontWeight.w500,),),
              Padding(padding: EdgeInsets.fromLTRB(0, 50, 0, 0),),
              CircularProgressIndicator(
                strokeWidth: 5,
                backgroundColor: Colors.cyanAccent,
                valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),
                value: _progress,
              ),
              Padding(padding: EdgeInsets.fromLTRB(0, 20, 0, 0),),
              RaisedButton(
                color: Colors.teal,
                child: Text('Lets See the Paintings',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.white),),
                onPressed: () {
                  setState(() {
                    _progress = 0;
                  });
                  startTimer();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}


/*class _FadeTransitionExample extends StatefulWidget {
  _FadeTransitionExampleState createState() => _FadeTransitionExampleState();
}

class _FadeTransitionExampleState extends State<_FadeTransitionExample> with TickerProviderStateMixin {

  AnimationController _controller;
  Animation<double> _animation;

  initState() {
    super.initState();

    _controller = AnimationController(
        duration: const Duration(milliseconds: 5000),
        vsync: this,
        value: 0,
        lowerBound: 0,
        upperBound: 1
    );
    _animation = CurvedAnimation(parent: _controller, curve: Curves.fastOutSlowIn);

    _controller.forward();
  }

  @override
  dispose() {
    _controller.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Woolha.com Flutter Tutorial'),
      ),
      body: Center(
        child: FadeTransition(
          opacity: _animation,
          child: Center(
            child: Text('Woolha.com', style: TextStyle(color: Colors.teal, fontSize: 50)),
          ),
        ),
      ),
    );
  }
}
*/