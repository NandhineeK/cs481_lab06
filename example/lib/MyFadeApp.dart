
import 'package:flutter/material.dart';
import 'package:cs481_lab06/image_fade.dart';
import 'package:cs481_lab06/main.dart';
import 'package:cs481_lab06/PhotoGalery.dart';

import 'main.dart';

class MyFadeApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final title = 'Fade in images';

    return MaterialApp(
      title: title,
      home: Scaffold(

        body:Second(),
        // Center(
        //   child:
        //   FadeInImage.assetNetwork(
        //     placeholder: 'assets/loading.gif',
        //     image: 'https://picsum.photos/250?image=9',
        //   ),
        // ),
     // ),
      ),
     );
  }
}

class Second extends StatefulWidget {
  Second({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _Second createState() => _Second();
}

class _Second extends State<Second> {
  // Sample images from Wikimedia Commons:
  static const List<String> _imgs = [
    'https://stylegesture.com/wp-content/uploads/2019/02/Simple-Watercolor-Painting-Ideas-For-Beginners-2-1.jpg',
    'https://watercolorpainting.com/staging/wp-content/uploads/2018/11/Surreal-Skies.png',
    'https://secure.img1-fg.wfcdn.com/im/45417902/resize-h800%5Ecompr-r85/4438/44387010/%2527Hawaiian+Sunset%2527+Acrylic+Painting+Print+on+Canvas.jpg',
    'https://i.pinimg.com/564x/c1/2f/18/c12f189dfc84589d8db8c64b693b7c32.jpg'  ];

  int _counter = 0;
  bool _clear = true;
  bool _error = false;

  void _incrementCounter() {
    setState(() {
      if (_clear || _error) { _clear = _error = false; }
      else { _counter = (_counter+1)%_imgs.length; }
    });
  }

  @override
  Widget build(BuildContext context) {
    ImageProvider image;
    if (_error) { image = NetworkImage('error.jpg'); }
    else if (!_clear) { image = NetworkImage(_imgs[_counter]); }

    return Scaffold(
      appBar: AppBar(
        title: Text('Image from My collection'),
        backgroundColor: Colors.red,
      ),

      body: Stack(children: <Widget>[
        Positioned.fill(child:
        ImageFade(
          image: image,
          placeholder: Container(
            color: Colors.black.withOpacity(0.8),
            child: Center(child: Icon(Icons.sentiment_satisfied, color: Colors.red.withOpacity(0.8), size: 148.0,)),
          ),
          alignment: Alignment.center,
          fit: BoxFit.cover,
          loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent event) {
            if (event == null) { return child; }
            return Center(
              child: CircularProgressIndicator(
                  value: event.expectedTotalBytes == null ? 0.0 : event.cumulativeBytesLoaded / event.expectedTotalBytes
              ),
            );
          },
        )
        )
      ]),

      floatingActionButton: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
        FloatingActionButton(
          heroTag: "one",
          onPressed: _incrementCounter,
          tooltip: 'Next',
          child: Icon(Icons.navigate_next),

          backgroundColor: Colors.green,
        ),
        SizedBox(width:10.0),
        FloatingActionButton(
          heroTag: "two",
          onPressed: () =>
        Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => new MyApp())),
          tooltip: 'Back',
          child: Icon(Icons.arrow_back),
          backgroundColor: Colors.red.withOpacity(0.9),

        ),
        SizedBox(width: 10,),
        FloatingActionButton(
          backgroundColor: Colors.blueAccent,
          heroTag: "three",
          onPressed: () => {Navigator.of(context).push(MaterialPageRoute(
            builder: ((context) => new PhotoGalery()),
          ))},
          child: Icon(Icons.info_outline),
        )
      ]),
    );
  }
}
